module CBD
import Mmap

export readcbd, writecbd

const MAGIC_NUMBER = hex2bytes("CBDF")
const VERSION = 1


function readcbd(filename; mmap=false)

    open(filename, "r") do f

        # parse identifier and select precision
        identifier = read!(f, zeros(UInt8, 8))
        identifier[1:2] == MAGIC_NUMBER ||
            @error "Not a CBD file (magic number not matching)." identifier[1:2]
        identifier[3] == VERSION ||
            @error "Unsupported version of CBD format." identifier[3]
        T = identifier[end] == 0x4 ? Float32 : identifier[end] == 0x8 ? Float64 :
            @error "Unsupported data type: Only 32-bit and 64-bit precision is supported."

        # read values from file
        N = Tuple(read(f, UInt64) for i=1:3)
        xmin = Tuple(read(f, Float64) for i=1:3)
        xmax = Tuple(read(f, Float64) for i=1:3)
        x1, x2, x3 = Tuple(read!(f, zeros(Float64, n)) for n=N)
        data = mmap ? Mmap.mmap(f, Array{T,3}, N) : read!(f, zeros(T, N))

        all(xmin[i] <= minimum(x) <= maximum(x) <= xmax[i] for (i,x)=enumerate((x1,x2,x3))) ||
            @warn "Coordinates outside extent of domain."

        data, x1, x2, x3, xmin, xmax
    end
end

function writecbd(filename, data, x1, x2, x3, xmin, xmax, T=eltype(data))

    ndims(data) == length(xmin) == length(xmax) == 3 ||
        @error "Data or extent of domain has wrong number of dimensions."
    size(data) == (length(x1), length(x2), length(x3)) ||
        @error "Length of coordinates incompatible with dimensions of data."
    all(xmin[i] <= minimum(x) <= maximum(x) <= xmax[i] for (i,x)=enumerate((x1,x2,x3))) ||
        @error "Coordinates outside extent of domain."
    T ∈ (Float32, Float64) ||
        @error "Only 32-bit and 64-bit precision output supported." T

    # build first value, describing the version of the file format that is used
    identifier = zeros(UInt8, 8)
    identifier[1:2] = MAGIC_NUMBER
    identifier[3]   = VERSION
    identifier[end] = UInt8(sizeof(T))

    # write values to file, converting where necessary
    open(filename, "w") do f
        write(f, identifier)
        write(f, convert.(UInt64, size(data))...)
        write(f, convert.(Float64, xmin)...)
        write(f, convert.(Float64, xmax)...)
        for x = (x1, x2, x3)
            write(f, convert(Array{Float64,1}, x))
        end
        write(f, convert(Array{T,3}, data))
    end
end

end
