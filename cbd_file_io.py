# coding: utf-8

CBD_MAGIC_NUMBER = "CBDF"
CBD_VERSION = 1

import numpy as np

def read_cbd(filename, mmap=False):

    with open(filename, "rb") as f:

        # parse identifier and select precision
        identifier = f.read(8)
        if identifier[0:2] != bytearray.fromhex(CBD_MAGIC_NUMBER):
            raise ValueError("Not a CBD file (magic number not matching).")
        if identifier[2:3] != bytearray([CBD_VERSION]): # this works in Python 2.7 too
            raise ValueError("Unsupported version of CBD format.")
        if not identifier[-1] in bytearray([4, 8]): # this works in Python 2.7 too
            raise ValueError("Unsupported data type: Only 32-bit and 64-bit precision is supported.")
        T = np.float32 if identifier[-1:] == bytearray([4]) else np.float64

        # read values from file
        N = tuple(np.fromfile(f, dtype=np.int64, count=3))
        xmin = tuple(np.fromfile(f, dtype=np.float64, count=3))
        xmax = tuple(np.fromfile(f, dtype=np.float64, count=3))
        x1, x2, x3 = (np.fromfile(f, dtype=np.float64, count=n) for n in N)
        if not mmap:
            data = np.fromfile(f, dtype=T, count=np.prod(N)).reshape(N, order="F")

        if not all(xmin[i] <= np.min(x) <= np.max(x) <= xmax[i] for (i,x) in enumerate((x1,x2,x3))):
            warn("Coordinates outside extent of domain.")

    if mmap:
        data = np.memmap(filename, dtype=T, shape=N, offset=8*(10+np.sum(N)), order="F", mode="r")

    return data, x1, x2, x3, xmin, xmax


def write_cbd(filename, data, x1, x2, x3, xmin, xmax, T=None):

    T = data.dtype if T == None else T # set default data type

    if not (data.ndim == len(xmin) == len(xmax) == 3):
        raise ValueError("Data or extent of domain has wrong number of dimensions.")
    if data.shape != (len(x1), len(x2), len(x3)):
        raise ValueError("Length of coordinates incompatible with dimensions of data.")
    if not all(xmin[i] <= np.min(x) <= np.max(x) <= xmax[i] for (i,x) in enumerate((x1,x2,x3))):
        raise ValueError("Coordinates outside extent of domain.")
    if not T in (np.float32, np.float64):
        raise ValueError("Only 32-bit and 64-bit precision output supported.")

    # build first value, describing the version of the file format that is used
    identifier = bytearray(8)
    identifier[0:2] = bytearray.fromhex(CBD_MAGIC_NUMBER)
    identifier[2] = CBD_VERSION
    identifier[-1] = np.dtype(T).itemsize

    # write values to file, converting where necessary
    with open(filename, "wb") as f:
        f.write(identifier)
        np.asarray(data.shape, dtype=np.uint64).tofile(f)
        np.asarray(xmin, dtype=np.float64).tofile(f)
        np.asarray(xmax, dtype=np.float64).tofile(f)
        for x in (x1, x2, x3):
            np.asarray(x, dtype=np.float64).tofile(f)
        # data has to be flattened to write it in Fortran order
        data.astype(T).flatten(order="F").tofile(f)
