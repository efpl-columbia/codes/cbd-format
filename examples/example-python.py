# coding: utf-8

import sys
sys.path.append('../')
from cbd_file_io import read_cbd, write_cbd

import numpy as np

write_cbd("example32-python.cbd", np.fromfunction(lambda i, j, k: 100*(k+1)+10*(j+1)+(i+1),
    (5, 4, 3), dtype=int), np.arange(1,6), np.arange(1,5), np.arange(1,4),
    (0,0,0), (9,9,9), np.float32)

write_cbd("example64-python.cbd", np.fromfunction(lambda i, j, k: 100*(k+1)+10*(j+1)+(i+1),
    (5, 4, 3), dtype=int), np.arange(1,6), np.arange(1,5), np.arange(1,4),
    (0,0,0), (9,9,9), np.float64)

ref_data = read_cbd("example32.cbd")[0]
data, x1, x2, x3, xmin, xmax = read_cbd("example32-python.cbd")
assert np.all(data == ref_data)
data, x1, x2, x3, xmin, xmax = read_cbd("example32-python.cbd", mmap=True)
assert np.all(data == ref_data)

ref_data = read_cbd("example64.cbd")[0]
data, x1, x2, x3, xmin, xmax = read_cbd("example64-python.cbd")
assert np.all(data == ref_data)
data, x1, x2, x3, xmin, xmax = read_cbd("example64-python.cbd", mmap=True)
assert np.all(data == ref_data)
