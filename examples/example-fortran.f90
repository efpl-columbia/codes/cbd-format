program cbd_example

    use cbd_file_io, only: read_cbd_file, write_cbd_file
    use iso_fortran_env, only: real32, real64

    implicit none

    logical :: verbose = .true.
    integer :: i, j, k
    character(2), dimension(2) :: p = (/ '32', '64' /)
    character(8) :: arg

    real(real64), dimension(3) :: xmin = (/ 0, 0, 0 /), xmax = (/ 9, 9, 9 /)
    real(real64), dimension(5) :: x1 = (/ (i, i=1,5) /)
    real(real64), dimension(4) :: x2 = (/ (i, i=1,4) /)
    real(real64), dimension(3) :: x3 = (/ (i, i=1,3) /)
    real(real64), dimension(5,4,3) :: data3d = reshape((/ (((100*k+10*j+i, &
                                i=1,5), j=1,4), k=1,3) /), (/ 5, 4, 3 /))

    real(real64), dimension(3) :: xmin_in, xmax_in
    real(real64), dimension(:), allocatable :: x1_in, x2_in, x3_in
    real(real64), dimension(:,:,:), allocatable :: data3d_in

    call get_command_argument(1, arg)
    if (arg == '--silent') verbose = .false.

    call write_cbd_file('example32-fortran.cbd', data3d, x1, x2, x3, xmin, xmax, real32)
    call write_cbd_file('example64-fortran.cbd', data3d, x1, x2, x3, xmin, xmax, real64)

    do i = 1,2
        if (verbose) write(*, *) 'Reading ' // p(i) // 'bit data file ...'
        call read_cbd_file("example32-fortran.cbd", data3d_in, x1_in, x2_in, x3_in, xmin_in, xmax_in)
        if (verbose) write(*, '("XMIN, XMAX:",/," (",3F4.1," ) <-> (",3F4.1," )")') &
            xmin_in, xmax_in
        if (verbose) write(*, '("X1, X2, X3:",/," (",5F4.1," )",/," (",4F4.1," )",/,&
            &" (",3F4.1," )")') x1_in, x2_in, x3_in
        if (verbose) write(*, '("DATA:",15(/,5F6.1))') data3d_in
    end do

end
