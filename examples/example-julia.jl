include("../cbd_file_io.jl")
using .CBD

for b = (32, 64), mmap = (false, true)

    fn = "example$(b)-julia.cbd"
    d = [100*k+10*j+i for i=1:5, j=1:4, k=1:3]

    writecbd(fn, d, 1:5, 1:4, 1:3, (0,0,0), (9,9,9), b==32 ? Float32 : Float64)
    data, x1, x2, x3, xmin, xmax = readcbd(fn, mmap=mmap)

    @assert (b == 32 ? isapprox : isequal)(data, d)
    @assert x1 == 1:5
    @assert x2 == 1:4
    @assert x3 == 1:3
    @assert xmin == (0,0,0)
    @assert xmax == (9,9,9)
end
